# StepApp REST API

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development purposes. 

### Installing

A step by step series of examples that tell you how to get a development env running


```
git clone git@gitlab.com:Gilberto97/stepapp.git
```
```
cd stepapp
```
```
docker-compose build
```
```
git checkout develop
```
```
docker-compose up
```
If some errors appears at that point, execute 'make dclean' and build docker again. 

To test if API works:
```
curl --request GET 'http://127.0.0.1:8000/api/users/' 
```


<!-- ## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
``` 

## Deployment

Add additional notes about how to deploy this on a live system -->

## Built With

* [Docker](https://www.docker.com/) - Tool to create, deploy, and run applications by using containers
* [Django](https://www.djangoproject.com/) - Python web framework
* [Django REST framework](https://www.django-rest-framework.org/) - Toolkit for building Web APIs
* [PostgreSQL](https://www.postgresql.org/) - Database


## Authors

* **Przemysław Baj** 

<!-- ## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details -->
