#!make
include .env
export

.SILENT:

.PHONY: help

test:	##test
	echo $(SQL_USER)

help:	## print the help message
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

clean:	## clean the project form temp files
	@-find . -name "__pycache__" -exec rm -rf {} \; 2>/dev/null || true
	@-find . -name "*.pyc" -exec rm -rf {} \; 2>/dev/null || true
	@-find . -name ".pytest_cache" -exec rm -rf {} \; 2>/dev/null || true
	@-find ../../ -name "pip-wheel-metadata" -exec rm -rf "{}" \; 2>/dev/null || true

dclean:	##	clean dockers
	docker stop `docker ps -a -q` ; 2>/dev/null || true
	docker rm `docker ps -a -q`; 2>/dev/null || true
	echo y | docker system prune -a
	docker images purge
	echo y | docker volume prune
	# docker network rm `docker network ls | grep "bridge" | awk '/ / { print $1 }'`

websh:	## connecting to web shell
	docker exec -it `docker ps -aq --filter "name=stepapp_web_"` /bin/sh

pyshell:	## connecting to manage.py shell
	docker exec -it `docker ps -aq --filter "name=stepapp_web_"` python manage.py shell

makemigrations:	## manage.py makemigrations
	docker exec -it `docker ps -aq --filter "name=stepapp_web_"` python manage.py makemigrations

migrate:	## manage.py migrate
	docker exec -it `docker ps -aq --filter "name=stepapp_web_"` python manage.py migrate

dbsh:	## connecting to db shell
	docker exec -it `docker ps -aq --filter "name=stepapp_db_"` /bin/sh

psql:	## connecting to db psql commandline
	docker exec -it `docker ps -aq --filter "name=stepapp_db_"` psql -d $(SQL_DATABASE) -U $(SQL_USER)

