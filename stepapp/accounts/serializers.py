from .models import User, Country
from orders.models import Order
from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from operator import itemgetter


class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Country
        fields = '__all__'

    def to_internal_value(self,data):
        return self()
        
class PasswordSerializer(serializers.Serializer):
    """
    Serializer for password change endpoint.
    """
    old_password = serializers.CharField()
    new_password = serializers.CharField()
    re_new_password = serializers.CharField()


class UserSerializer(serializers.ModelSerializer):
    # country = serializers.ChoiceField(tuple(Country.objects.values('code')))
    re_password = serializers.CharField(write_only=True)
    employee_type = serializers.ChoiceField(User.EMPLOYEE_TYPES)
    class Meta:
        model = User
        fields = ('id','employee_type','email','password','re_password','first_name','last_name','date_of_birth','description','address','phone','country','date_joined','is_staff','is_superuser')
        read_only_fields = ('id','date_jointed', 'is_staff','is_superuser'),
        extra_kwargs = {
            'password': {'write_only': True},
        }
    
    def validate_re_password(self, re_password):
        password = self.initial_data['password']
        if not password == re_password:
            raise serializers.ValidationError('Passwords must be the same.')

    def create(self, validated_data):
        phone = validated_data['phone']
        calling_code = validated_data['country'].calling_code
        if not phone.startswith(calling_code):
            raise serializers.ValidationError('Phone must starts with user country prefix.')

        password = validated_data.pop('password', None)
        validated_data.pop('re_password')
        user = User(**validated_data)
        user.set_password(password)
        user.save()
        return user

class MyUserSerializer(UserSerializer):
    my_orders = serializers.PrimaryKeyRelatedField(many=True, queryset=Order.objects.all())
    requested_orders = serializers.PrimaryKeyRelatedField(many=True, queryset=Order.objects.all())
    
    class Meta(UserSerializer.Meta):
        fields = UserSerializer.Meta.fields + ('my_orders', 'requested_orders')
        read_only_fields = UserSerializer.Meta.read_only_fields + ('my_orders', 'requested_orders')


