from django.urls import include, path
from rest_framework.routers import DefaultRouter
from .views import UserViewSet, CountryViewSet, UserDetailViewSet

app_name = 'stepapp'

router = DefaultRouter()
router.register(r'users', UserViewSet, basename='user')
router.register(r'my_user', UserDetailViewSet, basename='country')
router.register(r'countries', CountryViewSet, basename='country')

urlpatterns = router.urls
