from django.db import models
from django.conf import settings 
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
from django.db.models.signals import post_save
from django.utils import timezone
from django.contrib.auth.models import BaseUserManager, AbstractUser
from django.utils.translation import gettext as _

# Create your models here.

class Country(models.Model):
    code = models.CharField(primary_key=True, max_length=3) 
    calling_code = models.CharField(max_length=3, unique=True)
    name = models.CharField(max_length=50)
    currency = models.CharField(max_length=3)
    continent_name = models.CharField(max_length=20)


class UserManager(BaseUserManager):
    def create_user(self, email, password, **extra_fields):
        if not email:
            raise ValueError('Enter an email address')
        if not password:
            raise ValueError('Enter a password')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password, **extra_fields):
        user = self.create_user(email, password=password, **extra_fields)
        user.is_superuser = True
        user.is_staff = True
        user.save(using=self._db)
        return user


class User(AbstractUser):
    EMPLOYEE_TYPES = [
        ('R', 'Regular'),
        ('S', 'Specialist')
    ]
    username_validator = None
    username = None
    first_name = models.CharField(_('first name'), max_length=30)
    last_name = models.CharField(_('last name'), max_length=150)
    phone = models.CharField(max_length=15, unique=True)
    employee_type = models.CharField(max_length=1, choices=EMPLOYEE_TYPES, default='R')
    email = models.EmailField(_('email address'), unique=True, max_length=255)
    description = models.CharField(blank=True, max_length=500, default='')
    address = models.CharField(max_length=100)
    date_of_birth = models.DateField()
    country = models.ForeignKey(Country, on_delete=models.CASCADE, related_name='users')

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name', 'date_of_birth', 'address', 'phone', 'country']

    objects = UserManager()
