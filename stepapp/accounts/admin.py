from django.contrib import admin
from .models import Country, User

admin.site.register(Country)
admin.site.register(User)
# Register your models here.
