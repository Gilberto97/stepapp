from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework.viewsets import ModelViewSet
from django.core.mail import send_mail
from operator import itemgetter

from django.http import Http404
from rest_framework.views import APIView

from .models import User, Country
from .serializers import UserSerializer, MyUserSerializer, CountrySerializer, PasswordSerializer

class CountryViewSet(ModelViewSet):
    serializer_class = CountrySerializer
    queryset = Country.objects.all()


class UserViewSet(ModelViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all()
    permission_classes = []


    # def create(self, request, format=None):
    #     serializer = self.get_serializer(data=request.data)
        
    #     if serializer.is_valid():              
    #         serializer.save()
            
    #         return Response(serializer.data, status=status.HTTP_201_CREATED)
    #     return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
    @action(methods=['get'], detail=False, url_path='me', url_name='my_user')
    def my_user(self, request):
        user = User.objects.get(id=request.user.id)
        print(user)
        serializer = MyUserSerializer(user)
        return Response(serializer.data, status=status.HTTP_200_OK)
    
    @action(methods=['put'], detail=False, url_path='change-password', url_name='change_password')
    def change_password(self, request):
        serializer = PasswordSerializer(data=request.data)
        user = request.user

        if serializer.is_valid():
            old_password, new_password, re_new_password = serializer.data.values()

            if not user.check_password(old_password):
                return Response({'old_password': 'Wrong password.'}, status=status.HTTP_400_BAD_REQUEST)
            if not new_password == re_new_password:
                return Response({'new_password': 'New passwords must be the same.'}, status=status.HTTP_400_BAD_REQUEST)
            user.set_password(serializer.data['new_password'])
            user.save()
            return Response({'status': 'password set'}, status=status.HTTP_200_OK)
        return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)


        # user = request.user
        # my_orders = Order.objects.filter(owner=request.user.id)
        # accepted_orders = Order.objects.all(employees_requested=request.user.id)

        # if serializer.is_valid():
        #     return Response(serializer.data, status=status.HTTP_200_OK)
        # return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)

# my_orders = serializers.PrimaryKeyRelatedField(many=True, queryset=Order.objects.all())
# accepted_orders = serializers.PrimaryKeyRelatedField(many=True, queryset=Order.objects.all())




class UserDetailViewSet(ModelViewSet):
    serializer_class = MyUserSerializer
    queryset = User.objects.all()
