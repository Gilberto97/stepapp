#!/bin/sh


# if [ "$DATABASE" = "postgres" ]
# then
#     echo "Waiting for postgres..."

#     while ! nc -z $SQL_HOST $SQL_PORT; do
#       sleep 0.1
#     done

#     echo "PostgreSQL started"
# fi

# uncomment line below to clear database every docker launch
python manage.py flush --no-input
python manage.py makemigrations
python manage.py migrate
echo "from accounts.models import Country; print(\"Country exists\") if Country.objects.filter(code='616').exists() else Country.objects.create(code='616', calling_code='48', name='Poland',currency='PLN',continent_name='Europe')" | python manage.py shell
echo "from accounts.models import User; print(\"Admin exists\") if User.objects.filter(email='$SQL_MAIL').exists() else User.objects.create_superuser(email='$SQL_MAIL', password='$SQL_PASSWORD', date_of_birth='1997-10-13', employee_type='R', phone='48668700519', country_id='616')" | python manage.py shell
echo "from accounts.models import User; print(\"Test user exists\") if User.objects.filter(email='user1@gmail.com').exists() else User.objects.create_user(email='user1@gmail.com', password='test1', date_of_birth='1997-10-13', employee_type='R', phone='48000111222', country_id='616')" | python manage.py shell

exec "$@"
