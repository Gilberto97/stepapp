from django.db import models
from django.utils import timezone
from django.core.validators import MaxValueValidator, MinValueValidator

# from django.utils.translation import gettext as _

from accounts.models import User

# Create your models here.

class Category(models.Model):
    name = models.CharField(max_length=255)


class UserCategory(models.Model):
    user = models.ForeignKey(User, on_delete=models.PROTECT)
    category = models.ForeignKey(Category, on_delete=models.PROTECT)
    level = models.IntegerField(default=1)
    experience = models.IntegerField(default=0)
    rating = models.FloatField(default=0)


class Order(models.Model):
    name = models.CharField(max_length=255)
    description = models.CharField(blank=True, max_length=500, default='')
    price = models.DecimalField(max_digits=1000, decimal_places=2)
    work_time_hours = models.IntegerField(default=1, validators=[MinValueValidator(0)])
    owner = models.ForeignKey(User, related_name='my_orders', on_delete=models.PROTECT)
    employees_requested = models.ManyToManyField(User, related_name='requested_orders', default=[], blank=True)
    employee = models.ForeignKey(User, related_name='accepted_orders', on_delete=models.PROTECT, default=None, null=True)
    category = models.ForeignKey(Category, on_delete=models.PROTECT)
    coords_latitude = models.DecimalField(max_digits=8, decimal_places=6, validators=[MinValueValidator(-90), MaxValueValidator(90)])
    coords_longitude = models.DecimalField(max_digits=9, decimal_places=6, validators=[MinValueValidator(-180), MaxValueValidator(180)])
    created_at = models.DateTimeField(default=timezone.now)
    finished_at = models.DateTimeField(null=True, default=None)
