# Generated by Django 3.0.3 on 2020-03-01 11:40

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('orders', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='order',
            name='level',
        ),
        migrations.AlterField(
            model_name='order',
            name='finished_at',
            field=models.DateTimeField(null=True),
        ),
        migrations.AlterField(
            model_name='order',
            name='workers',
            field=models.ManyToManyField(null=True, related_name='workers', to=settings.AUTH_USER_MODEL),
        ),
    ]
