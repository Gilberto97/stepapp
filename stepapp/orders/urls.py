from django.urls import include, path
from rest_framework.routers import DefaultRouter
from .views import CategoryViewSet, OrderViewSet, UserOwnedOrdersViewSet, UserTakenOrdersViewSet

app_name = 'stepapp'

router = DefaultRouter()
router.register(r'categories', CategoryViewSet, basename='category')
router.register(r'orders', OrderViewSet, basename='order')
router.register(r'my_orders', UserOwnedOrdersViewSet, basename='my orders')
router.register(r'taken_orders', UserTakenOrdersViewSet, basename='taken orders')
urlpatterns = router.urls



