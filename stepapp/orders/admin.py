from django.contrib import admin
from .models import Order, Category, UserCategory

admin.site.register(Order)
admin.site.register(Category)
admin.site.register(UserCategory)

# Register your models here.
