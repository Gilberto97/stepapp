from rest_framework.permissions import SAFE_METHODS, BasePermission

class IsSuperuserOrReadOnly(BasePermission):
    
    """
    Allows access only to superusers if not readonly.
    """
    message = 'You must be admin to do it.'

    def has_permission(self, request, view):
        if request.method in SAFE_METHODS:
            return True
        return bool(request.user and request.user.is_superuser)

# class IsAdminOrIsSelf(BasePermission):

#     message = 'You must be admin or owner.'
    
#     def has_permission(self, request, view):
#         return bool(request.user and request.user.is_superuser)

#     def has_object_permission(self, request, view, obj):
#         if hasattr(obj, 'owner'):
#             return obj.owner == request.user 



class IsOwner(BasePermission):
    
    """
    Object-level permission to only allow owners of an object to edit it.
    Assumes the model instance has an `owner` attribute.
    """
    message = 'You must be the owner of this object.'

    def has_object_permission(self, request, view, obj):
        if hasattr(obj, 'owner'):
            return obj.owner == request.user
